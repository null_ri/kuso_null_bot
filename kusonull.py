#!/usr/local/pyenv/shims/ python
# -*- coding: utf-8 -*-

from requests_oauthlib import OAuth1Session
import time
import random

# ツイート間隔(秒)の設定
tweet_time = 60 * 60

# OAuthのKeyを取得 (メソッド化したい )
#f = open('/home/null_ri/program/kuso_null_keys', 'r', encoding='utf-8')
f = open('./program/kuso_null_keys', 'r', encoding='utf-8')
keys = f.readlines()
CK = keys[0].split('\n')[0]
CS = keys[1].split('\n')[0]
AT = keys[2].split('\n')[0]
AS = keys[3].split('\n')[0]
f.close()

# ツイートURLとOAuthKeyを設定(メソッド化したい )
url = "https://api.twitter.com/1.1/statuses/update.json"
twitter = OAuth1Session(CK, CS, AT, AS)

# ツイートリストの一覧を取得 (別ファイルに格納)(メソッド化したい )
f2 = open('./program/kuso_null_words', 'r', encoding='utf-8')
lines = f2.readlines()
i = 0
tweets = []
for word in lines:
    tweets.append( word.split('\n')[0] )


#-------------------------------------------------#


# APIに接続してツイートする
while True :
    while True :
        try:
            n = random.randrange(len(tweets))
            print(n)
            params = {"status": tweets[n]} 
            req = twitter.post(url, params = params)

            if req.status_code == 200:
                print("OK")
            else:
                print("Error: %d" % req.status_code)
                break
        except:
            print("tweet failed.")
            break
        else:
            # ツイート間隔(秒)
            time.sleep(tweet_time)
